package sqlite

import (
	"context"
	"database/sql"
	"io/ioutil"
	"os"
	"reflect"
	"testing"
	"time"

	"gitlab.com/maciej.dlugoszek/weatherapi"
)

var (
	testQuery = &weatherapi.WeatherQuery{
		City: "Warsaw",
	}
	testWeather = &weatherapi.Weather{
		Temperature:    10,
		MinTemperature: 1,
		MaxTemperature: 100,
	}
)

func TestSimpleAddGetQuery(t *testing.T) {
	f, err := ioutil.TempFile(".", "")
	if err != nil {
		t.Fatalf("could not create file for db: %v", err)
	}
	f.Close()
	defer os.Remove(f.Name())

	db, err := sql.Open("sqlite3", f.Name())
	if err != nil {
		t.Fatalf("could not open db: %v", err)
	}

	storage, err := NewQueryStorage(db)
	if err != nil {
		t.Fatalf("could not create storage: %v", err)
	}
	defer storage.Close()

	err = storage.Put(context.Background(), testQuery, testWeather)
	if err != nil {
		t.Fatalf("could not add bookmark to storage: %v", err)
	}

	stats, err := storage.Get(context.Background(), time.Now().Add(-time.Hour), time.Now().Add(time.Hour))
	if err != nil {
		t.Fatalf("could not get bookmarks: %v", err)
	}

	wantedStats := weatherapi.QueryStatistics{
		MinTemperature:     testWeather.MinTemperature,
		MaxTemperature:     testWeather.MaxTemperature,
		TotalQueries:       1,
		AverageTemperature: testWeather.Temperature,
		WeatherCounter: map[string]int{
			testWeather.Summary: 1,
		},
	}

	if !reflect.DeepEqual(*stats, wantedStats) {
		t.Fatalf("wanted %v , got %v", stats, wantedStats)
	}
}

func TestGetStatsWithoutData(t *testing.T) {
	f, err := ioutil.TempFile(".", "")
	if err != nil {
		t.Fatalf("could not create file for db: %v", err)
	}
	f.Close()
	defer os.Remove(f.Name())

	db, err := sql.Open("sqlite3", f.Name())
	if err != nil {
		t.Fatalf("could not open db: %v", err)
	}

	storage, err := NewQueryStorage(db)
	if err != nil {
		t.Fatalf("could not create storage: %v", err)
	}
	defer storage.Close()

	stats, err := storage.Get(context.Background(), time.Now().Add(-time.Hour), time.Now().Add(time.Hour))
	if err != nil {
		t.Fatalf("could not get bookmarks: %v", err)
	}

	wantedStats := weatherapi.QueryStatistics{
		MinTemperature:     0,
		MaxTemperature:     0,
		TotalQueries:       0,
		AverageTemperature: 0,
		WeatherCounter:     map[string]int{},
	}

	if !reflect.DeepEqual(*stats, wantedStats) {
		t.Fatalf("wanted %v , got %v", stats, wantedStats)
	}
}
