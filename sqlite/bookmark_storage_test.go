package sqlite

import (
	"context"
	"database/sql"
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/maciej.dlugoszek/weatherapi"

	_ "github.com/mattn/go-sqlite3" //imports sqlite driver
)

var (
	testBookmark = &weatherapi.Bookmark{
		Username: "Maciek",
		City:     "Moscow",
	}
)

func TestSimplePutGetBookmark(t *testing.T) {
	f, err := ioutil.TempFile(".", "")
	if err != nil {
		t.Fatalf("could not create file for db: %v", err)
	}
	f.Close()
	defer os.Remove(f.Name())

	db, err := sql.Open("sqlite3", f.Name())
	if err != nil {
		t.Fatalf("could not open db: %v", err)
	}

	storage, err := NewBookmarksStorage(db)
	if err != nil {
		t.Fatalf("could not create storage: %v", err)
	}
	defer storage.Close()

	err = storage.Put(context.Background(), testBookmark)
	if err != nil {
		t.Fatalf("could not Put bookmark to storage: %v", err)
	}

	bookmarks, err := storage.Get(context.Background(), testBookmark.Username)
	if err != nil {
		t.Fatalf("could not get bookmarks: %v", err)
	}

	if len(bookmarks) != 1 {
		t.Fatalf("wanted 1 bookmark, got %v", bookmarks)
	}

	if *testBookmark != bookmarks[0] {
		t.Fatalf("wanted %v , got %v", testBookmark, bookmarks[0])
	}
}
