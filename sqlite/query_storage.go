package sqlite

import (
	"context"
	"database/sql"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/maciej.dlugoszek/weatherapi"
)

//QueryStorage implements weatherpi.QueryStorage using SQLite database
type QueryStorage struct {
	db *sql.DB
}

//NewQueryStorage returns a new instance of a QueryStorage using SQLite at given path
func NewQueryStorage(db *sql.DB) (*QueryStorage, error) {
	queryStorage := &QueryStorage{
		db: db,
	}

	if err := queryStorage.initTable(); err != nil {
		return nil, errors.Wrap(err, "could not init QueryStorage")
	}

	return queryStorage, nil
}

func (s *QueryStorage) initTable() error {
	_, err := s.db.Exec(`
	create table if not exists query (
		id integer not null primary key,
		timestamp integer not null,
		queried_city  text not null,
		summary  text not null,
		temp  real not null,
		max_temp real not null,
		min_temp real not null,
		pressure real not null
	)`)
	return errors.Wrap(err, "could not create table")
}

//Put given query and weather to database
func (s *QueryStorage) Put(ctx context.Context, query *weatherapi.WeatherQuery, weather *weatherapi.Weather) error {
	if query == nil || weather == nil {
		return errors.Errorf("got nil input")
	}

	_, err := s.db.Exec(`
	insert into query (timestamp,queried_city,summary,temp,max_temp,min_temp,pressure) 
	values (?,?,?,?,?,?,?)
	`, time.Now().Local().Unix(), query.City, weather.Summary, weather.Temperature, weather.MaxTemperature, weather.MinTemperature, weather.Pressure)

	return errors.Wrap(err, "could not execute insert query")
}

//Get returns statistics about weather in given time interval
func (s *QueryStorage) Get(ctx context.Context, from, to time.Time) (*weatherapi.QueryStatistics, error) {
	stats, err := s.getBaseStats(ctx, from, to)
	if err != nil {
		return nil, errors.Wrap(err, "could not get base stats")
	}

	stats.WeatherCounter, err = s.getCounterMap(ctx, from, to)
	if err != nil {
		return nil, errors.Wrap(err, "could not get counter map")
	}

	return stats, nil
}

func (s *QueryStorage) getCounterMap(ctx context.Context, from, to time.Time) (map[string]int, error) {
	result := make(map[string]int)

	rows, err := s.db.Query(`
	select summary,count(*)
	from query
	where timestamp >= ? and timestamp <= ?
	group by summary`, from.Local().Unix(), to.Local().Unix())
	if err != nil {
		return nil, errors.Wrap(err, "could not perform query")
	}
	defer rows.Close()

	for rows.Next() {
		var summary string
		var ct int
		if err = rows.Scan(&summary, &ct); err != nil {
			return nil, errors.Wrap(err, "could not scan result")
		}
		result[summary] = ct
	}

	return result, nil
}

func (s *QueryStorage) getBaseStats(ctx context.Context, from, to time.Time) (*weatherapi.QueryStatistics, error) {
	stats := &weatherapi.QueryStatistics{}

	var AverageTemperature, MaxTemperature, MinTemperature, AveragePressure sql.NullFloat64 //Needed if there is no data

	err := s.db.QueryRow(`
	select count(*),avg(temp),max(max_temp),min(min_temp),avg(pressure)
	from query
	where timestamp >= ? and timestamp <= ?
	`, from.Local().Unix(), to.Local().Unix()).Scan(
		&stats.TotalQueries,
		&AverageTemperature,
		&MaxTemperature,
		&MinTemperature,
		&AveragePressure,
	)

	if AverageTemperature.Valid { //assume that if one value is correct -> all are ( all fields are not null so it makes)
		stats.AverageTemperature = AverageTemperature.Float64
		stats.MaxTemperature = MaxTemperature.Float64
		stats.MinTemperature = MinTemperature.Float64
		stats.AveragePressure = AveragePressure.Float64
	}

	if err != nil {
		return nil, errors.Wrap(err, "could not scan row")
	}
	return stats, nil
}

//Close internal database
func (s *QueryStorage) Close() error {
	return s.db.Close()
}
