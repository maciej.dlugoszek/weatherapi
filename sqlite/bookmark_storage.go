package sqlite

import (
	"context"
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.com/maciej.dlugoszek/weatherapi"
)

//BookmarksStorage implements weatherpi.BookmarksStorage using SQLite database
type BookmarksStorage struct {
	db *sql.DB
}

//NewBookmarksStorage returns a new instance of a BookmarksStorage using SQLite at given path
func NewBookmarksStorage(db *sql.DB) (*BookmarksStorage, error) {
	BookmarksStorage := &BookmarksStorage{
		db: db,
	}

	if err := BookmarksStorage.initTable(); err != nil {
		return nil, errors.Wrap(err, "could not init BookmarksStorage")
	}

	return BookmarksStorage, nil
}

func (s *BookmarksStorage) initTable() error {
	_, err := s.db.Exec(`
	create table if not exists bookmark (
		id integer not null primary key,
		username text,
		city text
	)`)
	return errors.Wrap(err, "could not create table")
}

//Put given bookmark to database
func (s *BookmarksStorage) Put(ctx context.Context, bookmark *weatherapi.Bookmark) error {
	if bookmark == nil {
		return errors.Errorf("got nil input")
	}

	_, err := s.db.Exec(`
	insert into bookmark (username,city) 
	values (?,?)
	`, bookmark.Username, bookmark.City)

	return errors.Wrap(err, "could not execute insert query")
}

//Get returns bookmarks of given user
func (s *BookmarksStorage) Get(ctx context.Context, username string) ([]weatherapi.Bookmark, error) {
	bookmarks := []weatherapi.Bookmark{}

	rows, err := s.db.Query(` 
	select city 
	from bookmark
	where username = ?`, username)
	if err != nil {
		return nil, errors.Wrap(err, "could not perform query")
	}
	defer rows.Close()

	for rows.Next() {
		var city string
		if err = rows.Scan(&city); err != nil {
			return nil, errors.Wrap(err, "could not scan result")
		}
		bookmarks = append(bookmarks, weatherapi.Bookmark{
			Username: username,
			City:     city,
		})
	}

	return bookmarks, nil
}

//Close internal database
func (s *BookmarksStorage) Close() error {
	return s.db.Close()
}
