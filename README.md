# WeatherAPI

## Installation

Installation consists of two steps.

### Step 1. Build Docker

You do not need to have Go installed. Go docker image will build the binary for you. All you need is

`docker build -t weather-api .` 

### Step 2. Run it !

Having the service installed, you can run it by typing following command: ( don't forget to set the API key for OpenWeatherMap)

`docker run -e OWM_API_KEY=<YOUR_API_KEY>  -p 8080:8080 weather-api`



## Server



### 1. Port

By default server listens on port 8080. It is automatically exposed on docker container. If you want to change it, supply an enviroment variable to the container/binary ( for example LISTEN=:8081 ). If you use docker, an additional port needs to be forwarded.

### 2. Endpoints

- GET /weather/city

  - q is a query param in which you specify which city's weather you want to check. 

    for example /weather/city?q=Warsaw

  - Example response

    `{`
        `"MaxTemperature": 13.33,`
        `"MinTemperature": 9.44,`
        `"Pressure": 1029,`
        `"Summary": "Clouds",`
        `"Temperature": 11.46`
    `}`

- GET /weather/bookmark

  - q is a query param in which you specify owner of bookmark 

    for example /weather/bookmark?q=Maciek

  - Example response 

    `[{`
        `"MaxTemperature": 13.33,`
        `"MinTemperature": 9.44,`
        `"Pressure": 1029,`
        `"Summary": "Clouds",`
        `"Temperature": 11.46`
    `}]`

- POST /weather/bookmark

  - Body consist of JSON with 2 mandatory fields

    - Username: owner of bookmark
    - City: city to save

  - Example body:

    `{`

    ​    `"Username":"Maciek",`

    ​    `"City":"Warsaw"`

    `}`

- GET /statistics/month

  - t is a query param in which you specify a timestamp. All received statistics are calculated based on data from a  period of month in which given timestamp belongs.

    for example /statistics/month?t=1553780671' will return statistics for March 2019

  -  Example response

    `{`
        `"AveragePressure": 1029,`
        `"AverageTemperature": 0,`
        `"MaxTemperature": 13.33,`
        `"MinTemperature": 9.44,`
        `"TotalQueries": 1,`
        `"WeatherCounter": {`
            `"Clouds": 1`
        `}`
    `}`

    
### 3. TooLateToFix

I noticed a few thing that I would change if I had more time:
- statistics endpoint could have 2 parameters: year and month ( more UX friendly than sending a timestamp)
- Invalid API key should result in more meaningful error
- Get /weather/bookmark should return a map of Place:Weather instead of a list of weathers