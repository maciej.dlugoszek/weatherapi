package openweathermap

type apiResponse struct {
	Main    mainResponse      `json:"main"`
	Weather []weatherResponse `json:"weather"`
}

type weatherResponse struct {
	Summary string `json:"main"`
}

type mainResponse struct {
	Temperature    float64 `json:"temp"`
	Pressure       float64 `json:"pressure"`
	MinTemperature float64 `json:"temp_min"`
	MaxTemperature float64 `json:"temp_max"`
}
