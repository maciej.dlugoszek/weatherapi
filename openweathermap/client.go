package openweathermap

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/maciej.dlugoszek/weatherapi"
)

//Client implements weatherapi.WeatherProvider using OpenWeatherMap API
type Client struct {
	apiKey string
}

//NewClient returns a new instance of a client filled with given apiKey
func NewClient(apiKey string) *Client {
	return &Client{
		apiKey: apiKey,
	}
}

//GetByCityName returns forecast for given city
func (c *Client) GetByCityName(ctx context.Context, cityName string) (*weatherapi.Weather, error) {
	url := fmt.Sprintf("http://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s&units=metric", cityName, c.apiKey)
	resp, err := http.Get(url)
	if err != nil {
		return nil, errors.Wrap(err, "could not perform get request")
	}
	defer resp.Body.Close()

	apiResp := &apiResponse{}

	if err = json.NewDecoder(resp.Body).Decode(apiResp); err != nil {
		return nil, errors.Wrap(err, "could not decode response")
	}

	if len(apiResp.Weather) == 0 {
		return nil, errors.Errorf("got empty weather response")
	}

	return &weatherapi.Weather{
		Summary:        apiResp.Weather[0].Summary,
		Temperature:    apiResp.Main.Temperature,
		MinTemperature: apiResp.Main.MinTemperature,
		MaxTemperature: apiResp.Main.MaxTemperature,
		Pressure:       apiResp.Main.Pressure,
	}, nil
}
