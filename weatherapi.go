package weatherapi

import (
	"context"
	"time"
)

//Weather describes a single piece of information about weather at given time
type Weather struct {
	Summary        string
	Temperature    float64
	Pressure       float64
	MinTemperature float64
	MaxTemperature float64
}

//WeatherQuery is a structure describing a query for weather
type WeatherQuery struct {
	City string
}

//QueryStatistics describes statistics about performed queries
type QueryStatistics struct {
	TotalQueries       int
	AverageTemperature float64
	MaxTemperature     float64
	MinTemperature     float64
	AveragePressure    float64
	WeatherCounter     map[string]int
}

//WeatherProvider is able to provide Weather information based on given query
type WeatherProvider interface {
	GetByCityName(ctx context.Context, cityName string) (*Weather, error)
}

//WeatherService provides all functionality for the API
type WeatherService interface {
	GetWeatherForCity(ctx context.Context, cityName string) (*Weather, error)
	GetWeatherForBookmarks(ctx context.Context, username string) ([]Weather, error)
	AddBookmark(ctx context.Context, bookmark *Bookmark) error
	GetStatisticsForMonth(ctx context.Context, month time.Time) (*QueryStatistics, error)
}

//QueryStorage is able to store queries and their results
type QueryStorage interface {
	Put(ctx context.Context, query *WeatherQuery, weather *Weather) error
	Get(ctx context.Context, from, to time.Time) (*QueryStatistics, error)
}
