package main

import (
	"database/sql"
	"log"
	"net/http"
	"os"

	"gitlab.com/maciej.dlugoszek/weatherapi/openweathermap"
	"gitlab.com/maciej.dlugoszek/weatherapi/service"

	"gitlab.com/maciej.dlugoszek/weatherapi/sqlite"

	_ "github.com/mattn/go-sqlite3" //imports sqlite driver
)

var (
	dbPath     = "./db.sql"
	listenPort = ":8080"
	apiKey     = "api-key-goes-here"
)

func init() {
	apiKey = os.Getenv("OWM_API_KEY")
	if val := os.Getenv("DB_PATH"); len(val) > 0 {
		dbPath = val
	}
	if val := os.Getenv("LISTEN"); len(val) > 0 {
		listenPort = val
	}
}

func main() {
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		log.Fatal(err)
	}
	bookmarksStorage, err := sqlite.NewBookmarksStorage(db)
	if err != nil {
		log.Fatal(err)
	}
	queryStorage, err := sqlite.NewQueryStorage(db)
	if err != nil {
		log.Fatal(err)
	}
	weatherProvider := openweathermap.NewClient(apiKey)

	srv := service.NewService(bookmarksStorage, queryStorage, weatherProvider)
	api := service.NewAPI(srv)
	http.ListenAndServe(listenPort, api.Mount())
}
