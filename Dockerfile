
FROM golang:1.12 as builder

# Set the Current Working Directory inside the container
WORKDIR /go/src/gitlab.com/maciej.dlugoszek/weatherapi

# Copy everything from the current directory to the PWD(Present Working Directory) inside the container
COPY . .

# Download dependencies
ENV GO111MODULE=on
RUN go mod vendor

# Build the Go app

WORKDIR /go/src/gitlab.com/maciej.dlugoszek/weatherapi/cmd/weatherAPI
RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -a -o /go/bin/weatherapi .


######## Lightweight image for running server #######
FROM frolvlad/alpine-glibc  

RUN apk --no-cache add ca-certificates


WORKDIR /root/

# Copy the Pre-built binary file from the previous stage
COPY --from=builder /go/bin/weatherapi .

EXPOSE 8080
RUN chmod +x ./weatherapi
CMD ["./weatherapi"] 