module gitlab.com/maciej.dlugoszek/weatherapi

go 1.12

require (
	github.com/briandowns/openweathermap v0.0.0-20180804155945-5f41b7c9d92d // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.3.0
	github.com/vektra/mockery v0.0.0-20181123154057-e78b021dcbb5 // indirect
	golang.org/x/crypto v0.0.0-20190325154230-a5d413f7728c // indirect
	golang.org/x/net v0.0.0-20190327214358-63eda1eb0650 // indirect
	golang.org/x/sys v0.0.0-20190322080309-f49334f85ddc // indirect
	golang.org/x/tools v0.0.0-20190328030505-8f05a32dce9f // indirect
)
