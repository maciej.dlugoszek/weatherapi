package service

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/maciej.dlugoszek/weatherapi"
)

//API allows to communicate with WeatherService via HTTP API
type API struct {
	service weatherapi.WeatherService
}

//NewAPI returns a new API instance using given WeatherService
func NewAPI(service weatherapi.WeatherService) *API {
	return &API{
		service: service,
	}
}

//Mount endpoints on given methods and return a handler
func (a *API) Mount() http.Handler {
	r := chi.NewRouter()

	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/weather/city", a.getWeatherForCityHandler)
	r.Get("/weather/bookmark", a.getWeatherForBookmarksHandler)
	r.Post("/weather/bookmark", a.addBookmarkHandler)

	r.Get("/statistics/month", a.getStatisticsForMonthHandler)

	return r
}

func (a *API) getWeatherForCityHandler(w http.ResponseWriter, r *http.Request) {
	city := r.URL.Query().Get("q")
	if len(city) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(newAPIError("empty q param"))
		return
	}

	weather, err := a.service.GetWeatherForCity(r.Context(), city)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(newAPIError(err.Error()))
		return
	}

	body, err := json.Marshal(weather)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(newAPIError(fmt.Sprintf("could not marshal weather: %v", err)))
		return
	}

	w.Write(body)
	w.WriteHeader(http.StatusOK)
}

func (a *API) getWeatherForBookmarksHandler(w http.ResponseWriter, r *http.Request) {
	username := r.URL.Query().Get("q")
	if len(username) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(newAPIError("empty q param"))
		return
	}

	weather, err := a.service.GetWeatherForBookmarks(r.Context(), username)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(newAPIError(err.Error()))
		return
	}

	body, err := json.Marshal(weather)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(newAPIError(fmt.Sprintf("could not marshal weather: %v", err)))
		return
	}

	w.Write(body)
	w.WriteHeader(http.StatusOK)
}

func (a *API) addBookmarkHandler(w http.ResponseWriter, r *http.Request) {
	bookmark := &weatherapi.Bookmark{}
	err := json.NewDecoder(r.Body).Decode(bookmark)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(newAPIError(fmt.Sprintf("could not marshal weather: %v", err)))
	}

	err = a.service.AddBookmark(r.Context(), bookmark)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(newAPIError(err.Error()))
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func (a *API) getStatisticsForMonthHandler(w http.ResponseWriter, r *http.Request) {
	t := r.URL.Query().Get("t")
	timestamp, err := strconv.Atoi(t)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write(newAPIError("could not parse t as timestamp"))
		return
	}

	statistics, err := a.service.GetStatisticsForMonth(r.Context(), time.Unix(int64(timestamp), 0))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(newAPIError(err.Error()))
		return
	}

	body, err := json.Marshal(statistics)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(newAPIError(fmt.Sprintf("could not marshal statistics: %v", err)))
		return
	}

	w.Write(body)
	w.WriteHeader(http.StatusOK)
}

func newAPIError(message string) []byte {
	err := struct {
		Error string
	}{
		Error: message,
	}
	buff, _ := json.Marshal(err)
	return buff
}
