package service

import (
	"context"
	"errors"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/maciej.dlugoszek/weatherapi"
	"gitlab.com/maciej.dlugoszek/weatherapi/mocks"
)

func TestService_AddBookmark(t *testing.T) {
	bookmarksStorageOK := new(mocks.BookmarkStorage)
	bookmarksStorageOK.On("Put", mock.Anything, mock.Anything).Return(nil)
	bookmarksStorageErr := new(mocks.BookmarkStorage)
	bookmarksStorageErr.On("Put", mock.Anything, mock.Anything).Return(errors.New("error"))

	type fields struct {
		bookmarksStorage weatherapi.BookmarkStorage
		queryStorage     weatherapi.QueryStorage
		weatherProvider  weatherapi.WeatherProvider
	}
	type args struct {
		ctx      context.Context
		bookmark *weatherapi.Bookmark
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "bookmarks storage runs fine -> no error",
			fields: fields{
				bookmarksStorage: bookmarksStorageOK,
			},
			wantErr: false,
		},
		{
			name: "bookmarks storage runs with err -> error expected",
			fields: fields{
				bookmarksStorage: bookmarksStorageErr,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(
				tt.fields.bookmarksStorage,
				tt.fields.queryStorage,
				tt.fields.weatherProvider,
			)
			if err := s.AddBookmark(tt.args.ctx, tt.args.bookmark); (err != nil) != tt.wantErr {
				t.Errorf("Service.AddBookmark() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestService_GetStatisticsForMonth(t *testing.T) {
	queryStorageOK := new(mocks.QueryStorage)
	queryStorageOK.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(exampleStats, nil)
	queryStorageErr := new(mocks.QueryStorage)
	queryStorageErr.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("error"))
	type fields struct {
		bookmarksStorage weatherapi.BookmarkStorage
		queryStorage     weatherapi.QueryStorage
		weatherProvider  weatherapi.WeatherProvider
	}
	type args struct {
		ctx   context.Context
		month time.Time
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *weatherapi.QueryStatistics
		wantErr bool
	}{

		{
			name: "query storage runs fine -> no error",
			fields: fields{
				queryStorage: queryStorageOK,
			},
			wantErr: false,
			want:    exampleStats,
		},
		{
			name: "query storage runs with err -> error expected",
			fields: fields{
				queryStorage: queryStorageErr,
			},
			wantErr: true,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(
				tt.fields.bookmarksStorage,
				tt.fields.queryStorage,
				tt.fields.weatherProvider,
			)
			got, err := s.GetStatisticsForMonth(tt.args.ctx, tt.args.month)
			if (err != nil) != tt.wantErr {
				t.Errorf("Service.GetStatisticsForMonth() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.GetStatisticsForMonth() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestService_GetWeatherForBookmarks(t *testing.T) {
	WeatherProviderOK := new(mocks.WeatherProvider)
	WeatherProviderOK.On("GetByCityName", mock.Anything, mock.Anything, mock.Anything).Return(exampleWeather, nil)
	WeatherProviderErr := new(mocks.WeatherProvider)
	WeatherProviderErr.On("GetByCityName", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("error"))

	QueryStorageOK := new(mocks.QueryStorage)
	QueryStorageOK.On("Put", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	QueryStorageErr := new(mocks.QueryStorage)
	QueryStorageErr.On("Put", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("error"))

	BookmarkStorageOK := new(mocks.BookmarkStorage)
	BookmarkStorageOK.On("Get", mock.Anything, mock.Anything, mock.Anything).Return([]weatherapi.Bookmark{*exampleBookmark}, nil)
	BookmarkStorageErr := new(mocks.BookmarkStorage)
	BookmarkStorageErr.On("Get", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("error"))

	type fields struct {
		bookmarksStorage weatherapi.BookmarkStorage
		queryStorage     weatherapi.QueryStorage
		weatherProvider  weatherapi.WeatherProvider
	}
	type args struct {
		ctx      context.Context
		username string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []weatherapi.Weather
		wantErr bool
	}{
		{
			name: "all components behaved correctly -> no error",
			fields: fields{
				bookmarksStorage: BookmarkStorageOK,
				queryStorage:     QueryStorageOK,
				weatherProvider:  WeatherProviderOK,
			},
			want:    []weatherapi.Weather{*exampleWeather},
			wantErr: false,
		},
		{
			name: "bookmark storage failed -> error",
			fields: fields{
				bookmarksStorage: BookmarkStorageErr,
				queryStorage:     QueryStorageOK,
				weatherProvider:  WeatherProviderOK,
			},
			wantErr: true,
			want:    nil,
		},
		{
			name: "query storage failed -> error",
			fields: fields{
				bookmarksStorage: BookmarkStorageOK,
				queryStorage:     QueryStorageErr,
				weatherProvider:  WeatherProviderOK,
			},
			wantErr: true,
			want:    nil,
		},
		{
			name: "weather provider failed -> error",
			fields: fields{
				bookmarksStorage: BookmarkStorageOK,
				queryStorage:     QueryStorageOK,
				weatherProvider:  WeatherProviderErr,
			},
			wantErr: true,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(
				tt.fields.bookmarksStorage,
				tt.fields.queryStorage,
				tt.fields.weatherProvider,
			)
			got, err := s.GetWeatherForBookmarks(tt.args.ctx, tt.args.username)
			if (err != nil) != tt.wantErr {
				t.Errorf("Service.GetWeatherForBookmarks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.GetWeatherForBookmarks() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestService_GetWeatherForCity(t *testing.T) {
	WeatherProviderOK := new(mocks.WeatherProvider)
	WeatherProviderOK.On("GetByCityName", mock.Anything, mock.Anything, mock.Anything).Return(exampleWeather, nil)
	WeatherProviderErr := new(mocks.WeatherProvider)
	WeatherProviderErr.On("GetByCityName", mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("error"))

	QueryStorageOK := new(mocks.QueryStorage)
	QueryStorageOK.On("Put", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	QueryStorageErr := new(mocks.QueryStorage)
	QueryStorageErr.On("Put", mock.Anything, mock.Anything, mock.Anything).Return(errors.New("error"))

	type fields struct {
		bookmarksStorage weatherapi.BookmarkStorage
		queryStorage     weatherapi.QueryStorage
		weatherProvider  weatherapi.WeatherProvider
	}
	type args struct {
		ctx      context.Context
		cityName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *weatherapi.Weather
		wantErr bool
	}{
		{
			name: "all components behaved correctly -> no error",
			fields: fields{
				queryStorage:    QueryStorageOK,
				weatherProvider: WeatherProviderOK,
			},
			want:    exampleWeather,
			wantErr: false,
		},
		{
			name: "query storage failed -> error",
			fields: fields{
				queryStorage:    QueryStorageErr,
				weatherProvider: WeatherProviderOK,
			},
			wantErr: true,
			want:    nil,
		},
		{
			name: "weather provider failed -> error",
			fields: fields{
				queryStorage:    QueryStorageOK,
				weatherProvider: WeatherProviderErr,
			},
			wantErr: true,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewService(
				tt.fields.bookmarksStorage,
				tt.fields.queryStorage,
				tt.fields.weatherProvider,
			)
			got, err := s.GetWeatherForCity(tt.args.ctx, tt.args.cityName)
			if (err != nil) != tt.wantErr {
				t.Errorf("Service.GetWeatherForCity() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Service.GetWeatherForCity() = %v, want %v", got, tt.want)
			}
		})
	}
}
