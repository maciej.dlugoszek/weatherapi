package service

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/maciej.dlugoszek/weatherapi"
)

//Service implements weatherapi.WeatherService using inner dependencies
type Service struct {
	bookmarksStorage weatherapi.BookmarkStorage
	queryStorage     weatherapi.QueryStorage
	weatherProvider  weatherapi.WeatherProvider
}

//NewService returns a new instance of a service with dependencies filled
func NewService(bookmarks weatherapi.BookmarkStorage, query weatherapi.QueryStorage, weather weatherapi.WeatherProvider) *Service {
	return &Service{
		bookmarksStorage: bookmarks,
		queryStorage:     query,
		weatherProvider:  weather,
	}
}

//GetWeatherForCity returns  current weather in given city
func (s *Service) GetWeatherForCity(ctx context.Context, cityName string) (*weatherapi.Weather, error) {
	weather, err := s.weatherProvider.GetByCityName(ctx, cityName)
	if err != nil {
		return nil, errors.Wrap(err, "could not get weather from provider")
	}

	err = s.queryStorage.Put(ctx, &weatherapi.WeatherQuery{City: cityName}, weather)
	if err != nil {
		return nil, errors.Wrap(err, "could not store query")
	}

	return weather, nil
}

//GetWeatherForBookmarks returns current weather for all bookmarked cities
func (s *Service) GetWeatherForBookmarks(ctx context.Context, username string) ([]weatherapi.Weather, error) {
	bookmarks, err := s.bookmarksStorage.Get(ctx, username)
	if err != nil {
		return nil, errors.Wrap(err, "could not get bookmarks")
	}

	result := []weatherapi.Weather{}

	for _, bookmark := range bookmarks {
		weather, err := s.GetWeatherForCity(ctx, bookmark.City)
		if err != nil {
			return nil, errors.Wrap(err, "could not get weather for city")
		}
		result = append(result, *weather)
	}

	return result, nil
}

//AddBookmark adds given bookmark to storage
func (s *Service) AddBookmark(ctx context.Context, bookmark *weatherapi.Bookmark) error {
	return errors.Wrap(s.bookmarksStorage.Put(ctx, bookmark), "could not add bookmark")
}

//GetStatisticsForMonth returns statistics for given month
func (s *Service) GetStatisticsForMonth(ctx context.Context, month time.Time) (*weatherapi.QueryStatistics, error) {
	stats, err := s.queryStorage.Get(
		ctx,
		time.Date(month.Year(), month.Month(), 0, 0, 0, 0, 0, time.Local),
		time.Date(month.Year(), month.Month()+1, 0, 0, 0, 0, 0, time.Local),
	)
	if err != nil {
		return nil, errors.Wrap(err, "could not get statistics")
	}

	return stats, nil
}
