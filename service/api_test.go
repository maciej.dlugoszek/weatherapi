package service

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/maciej.dlugoszek/weatherapi"
	"gitlab.com/maciej.dlugoszek/weatherapi/mocks"
)

var (
	exampleWeather = &weatherapi.Weather{
		Temperature:    10,
		MinTemperature: 5,
		MaxTemperature: 15,
		Pressure:       999,
	}

	exampleStats = &weatherapi.QueryStatistics{
		AverageTemperature: 11,
		MinTemperature:     5,
		MaxTemperature:     15,
		AveragePressure:    990,
	}

	exampleBookmark = &weatherapi.Bookmark{
		Username: "Maciek",
		City:     "Warsaw",
	}
)

func TestAPI_getWeatherForCityHandler(t *testing.T) {
	WeatherServiceOK := new(mocks.WeatherService)
	WeatherServiceOK.On("GetWeatherForCity", mock.Anything, mock.Anything).Return(exampleWeather, nil)
	WeatherServiceErr := new(mocks.WeatherService)
	WeatherServiceErr.On("GetWeatherForCity", mock.Anything, mock.Anything).Return(nil, errors.New("error"))

	type fields struct {
		service weatherapi.WeatherService
	}
	type args struct {
		query string
	}
	type want struct {
		body interface{}
		code int
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   want
	}{
		{
			name: "correct query and service result -> 200 and weather in response",
			fields: fields{
				service: WeatherServiceOK,
			},
			args: args{
				query: "warsaw",
			},
			want: want{
				code: http.StatusOK,
				body: exampleWeather,
			},
		},
		{
			name: "empty query -> 400 and appropriate error",
			fields: fields{
				service: WeatherServiceOK,
			},
			args: args{
				query: "",
			},
			want: want{
				code: http.StatusBadRequest,
				body: struct {
					Error string
				}{
					Error: "empty q param",
				},
			},
		},
		{
			name: "service error -> 500 and appropriate error",
			fields: fields{
				service: WeatherServiceErr,
			},
			args: args{
				query: "warsaw",
			},
			want: want{
				code: http.StatusInternalServerError,
				body: struct {
					Error string
				}{
					Error: "error",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wantedBody, err := json.Marshal(tt.want.body)
			if err != nil {
				t.Fatal(err)
			}

			req, err := http.NewRequest("GET", fmt.Sprintf("/weather/city?q=%s", tt.args.query), nil)
			if err != nil {
				t.Fatal(err)
			}

			api := NewAPI(tt.fields.service)
			rr := httptest.NewRecorder()
			api.Mount().ServeHTTP(rr, req)

			if rr.Code != tt.want.code {
				t.Fatalf("got bad code, wanted %v got %v", tt.want.code, rr.Code)
			}

			if bytes.Compare(rr.Body.Bytes(), wantedBody) != 0 {
				t.Fatalf("got bad body, wanted %v got %v", string(wantedBody), rr.Body.String())
			}

		})
	}
}

func TestAPI_getWeatherForBookmarksHandler(t *testing.T) {
	WeatherServiceOK := new(mocks.WeatherService)
	WeatherServiceOK.On("GetWeatherForBookmarks", mock.Anything, mock.Anything).Return([]weatherapi.Weather{*exampleWeather}, nil)
	WeatherServiceErr := new(mocks.WeatherService)
	WeatherServiceErr.On("GetWeatherForBookmarks", mock.Anything, mock.Anything).Return(nil, errors.New("error"))

	type fields struct {
		service weatherapi.WeatherService
	}
	type args struct {
		query string
	}
	type want struct {
		body interface{}
		code int
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   want
	}{
		{
			name: "correct query and service result -> 200 and weather in response",
			fields: fields{
				service: WeatherServiceOK,
			},
			args: args{
				query: "warsaw",
			},
			want: want{
				code: http.StatusOK,
				body: []weatherapi.Weather{*exampleWeather},
			},
		},
		{
			name: "empty query -> 400 and appropriate error",
			fields: fields{
				service: WeatherServiceOK,
			},
			args: args{
				query: "",
			},
			want: want{
				code: http.StatusBadRequest,
				body: struct {
					Error string
				}{
					Error: "empty q param",
				},
			},
		},
		{
			name: "service error -> 500 and appropriate error",
			fields: fields{
				service: WeatherServiceErr,
			},
			args: args{
				query: "warsaw",
			},
			want: want{
				code: http.StatusInternalServerError,
				body: struct {
					Error string
				}{
					Error: "error",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wantedBody, err := json.Marshal(tt.want.body)
			if err != nil {
				t.Fatal(err)
			}

			req, err := http.NewRequest("GET", fmt.Sprintf("/weather/bookmark?q=%s", tt.args.query), nil)
			if err != nil {
				t.Fatal(err)
			}

			api := NewAPI(tt.fields.service)
			rr := httptest.NewRecorder()
			api.Mount().ServeHTTP(rr, req)

			if rr.Code != tt.want.code {
				t.Fatalf("got bad code, wanted %v got %v", tt.want.code, rr.Code)
			}

			if bytes.Compare(rr.Body.Bytes(), wantedBody) != 0 {
				t.Fatalf("got bad body, wanted %v got %v", string(wantedBody), rr.Body.String())
			}

		})
	}
}

func TestAPI_addBookmarkHandler(t *testing.T) {
	WeatherServiceOK := new(mocks.WeatherService)
	WeatherServiceOK.On("AddBookmark", mock.Anything, mock.Anything).Return(nil)
	WeatherServiceErr := new(mocks.WeatherService)
	WeatherServiceErr.On("AddBookmark", mock.Anything, mock.Anything).Return(errors.New("error"))

	type fields struct {
		service weatherapi.WeatherService
	}
	type args struct {
		body interface{}
	}
	type want struct {
		code int
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   want
	}{
		{
			name: "correct query and service result -> 200 and weather in response",
			fields: fields{
				service: WeatherServiceOK,
			},
			args: args{
				body: exampleBookmark,
			},
			want: want{
				code: http.StatusCreated,
			},
		},
		{
			name: "empty query -> 400 and appropriate error",
			fields: fields{
				service: WeatherServiceOK,
			},
			args: args{
				body: "invalid body",
			},
			want: want{
				code: http.StatusBadRequest,
			},
		},
		{
			name: "service error -> 500 and appropriate error",
			fields: fields{
				service: WeatherServiceErr,
			},
			args: args{
				body: exampleBookmark,
			},
			want: want{
				code: http.StatusInternalServerError,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			requestBody, err := json.Marshal(tt.args.body)
			if err != nil {
				t.Fatal(err)
			}

			req, err := http.NewRequest("POST", "/weather/bookmark", bytes.NewReader(requestBody))
			if err != nil {
				t.Fatal(err)
			}

			api := NewAPI(tt.fields.service)
			rr := httptest.NewRecorder()
			api.Mount().ServeHTTP(rr, req)

			if rr.Code != tt.want.code {
				t.Fatalf("got bad code, wanted %v got %v", tt.want.code, rr.Code)
			}
		})
	}
}

func TestAPI_getStatisticsForMonthHandler(t *testing.T) {
	WeatherServiceOK := new(mocks.WeatherService)
	WeatherServiceOK.On("GetStatisticsForMonth", mock.Anything, mock.Anything).Return(exampleStats, nil)
	WeatherServiceErr := new(mocks.WeatherService)
	WeatherServiceErr.On("GetStatisticsForMonth", mock.Anything, mock.Anything).Return(nil, errors.New("error"))

	type fields struct {
		service weatherapi.WeatherService
	}
	type args struct {
		query string
	}
	type want struct {
		body interface{}
		code int
	}

	tests := []struct {
		name   string
		fields fields
		args   args
		want   want
	}{
		{
			name: "correct query and service result -> 200 and stats in response",
			fields: fields{
				service: WeatherServiceOK,
			},
			args: args{
				query: "1553783920",
			},
			want: want{
				code: http.StatusOK,
				body: exampleStats,
			},
		},
		{
			name: "incorrect timestamp  -> 400 and appropriate error",
			fields: fields{
				service: WeatherServiceOK,
			},
			args: args{
				query: "adsasd",
			},
			want: want{
				code: http.StatusBadRequest,
				body: struct {
					Error string
				}{
					Error: "could not parse t as timestamp",
				},
			},
		},
		{
			name: "service error -> 500 and appropriate error",
			fields: fields{
				service: WeatherServiceErr,
			},
			args: args{
				query: "1553783920",
			},
			want: want{
				code: http.StatusInternalServerError,
				body: struct {
					Error string
				}{
					Error: "error",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			wantedBody, err := json.Marshal(tt.want.body)
			if err != nil {
				t.Fatal(err)
			}

			req, err := http.NewRequest("GET", fmt.Sprintf("/statistics/month?t=%s", tt.args.query), nil)
			if err != nil {
				t.Fatal(err)
			}

			api := NewAPI(tt.fields.service)
			rr := httptest.NewRecorder()
			api.Mount().ServeHTTP(rr, req)

			if rr.Code != tt.want.code {
				t.Fatalf("got bad code, wanted %v got %v", tt.want.code, rr.Code)
			}

			if bytes.Compare(rr.Body.Bytes(), wantedBody) != 0 {
				t.Fatalf("got bad body, wanted %v got %v", string(wantedBody), rr.Body.String())
			}

		})
	}
}
