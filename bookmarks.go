package weatherapi

import (
	"context"
)

//Bookmark describes a single bookmark
type Bookmark struct {
	Username string
	City     string
}

//BookmarkStorage is able to store and retrieve bookmarks tied to given user
type BookmarkStorage interface {
	Get(ctx context.Context, username string) ([]Bookmark, error)
	Put(ctx context.Context, bookmark *Bookmark) error
}
