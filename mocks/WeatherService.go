// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import context "context"
import mock "github.com/stretchr/testify/mock"
import time "time"
import weatherapi "gitlab.com/maciej.dlugoszek/weatherapi"

// WeatherService is an autogenerated mock type for the WeatherService type
type WeatherService struct {
	mock.Mock
}

// AddBookmark provides a mock function with given fields: ctx, bookmark
func (_m *WeatherService) AddBookmark(ctx context.Context, bookmark *weatherapi.Bookmark) error {
	ret := _m.Called(ctx, bookmark)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *weatherapi.Bookmark) error); ok {
		r0 = rf(ctx, bookmark)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// GetStatisticsForMonth provides a mock function with given fields: ctx, month
func (_m *WeatherService) GetStatisticsForMonth(ctx context.Context, month time.Time) (*weatherapi.QueryStatistics, error) {
	ret := _m.Called(ctx, month)

	var r0 *weatherapi.QueryStatistics
	if rf, ok := ret.Get(0).(func(context.Context, time.Time) *weatherapi.QueryStatistics); ok {
		r0 = rf(ctx, month)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*weatherapi.QueryStatistics)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, time.Time) error); ok {
		r1 = rf(ctx, month)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWeatherForBookmarks provides a mock function with given fields: ctx, username
func (_m *WeatherService) GetWeatherForBookmarks(ctx context.Context, username string) ([]weatherapi.Weather, error) {
	ret := _m.Called(ctx, username)

	var r0 []weatherapi.Weather
	if rf, ok := ret.Get(0).(func(context.Context, string) []weatherapi.Weather); ok {
		r0 = rf(ctx, username)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]weatherapi.Weather)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, username)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWeatherForCity provides a mock function with given fields: ctx, cityName
func (_m *WeatherService) GetWeatherForCity(ctx context.Context, cityName string) (*weatherapi.Weather, error) {
	ret := _m.Called(ctx, cityName)

	var r0 *weatherapi.Weather
	if rf, ok := ret.Get(0).(func(context.Context, string) *weatherapi.Weather); ok {
		r0 = rf(ctx, cityName)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*weatherapi.Weather)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, cityName)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
