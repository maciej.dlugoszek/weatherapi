// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import context "context"
import mock "github.com/stretchr/testify/mock"
import weatherapi "gitlab.com/maciej.dlugoszek/weatherapi"

// BookmarkStorage is an autogenerated mock type for the BookmarkStorage type
type BookmarkStorage struct {
	mock.Mock
}

// Put provides a mock function with given fields: ctx, bookmark
func (_m *BookmarkStorage) Put(ctx context.Context, bookmark *weatherapi.Bookmark) error {
	ret := _m.Called(ctx, bookmark)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *weatherapi.Bookmark) error); ok {
		r0 = rf(ctx, bookmark)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Get provides a mock function with given fields: ctx, username
func (_m *BookmarkStorage) Get(ctx context.Context, username string) ([]weatherapi.Bookmark, error) {
	ret := _m.Called(ctx, username)

	var r0 []weatherapi.Bookmark
	if rf, ok := ret.Get(0).(func(context.Context, string) []weatherapi.Bookmark); ok {
		r0 = rf(ctx, username)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]weatherapi.Bookmark)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(context.Context, string) error); ok {
		r1 = rf(ctx, username)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
